
/*
  ledClass.h - Library for controlling array of LEDs
  Created by Ratitutorials.com, November 10, 2016
  Released into the public domain.
*/
#include <Arduino.h>
#include <EEPROM.h>
#include "ledClass.h"

// Constructor
ledClass::ledClass(){
  init();
}

const byte ledClass::_pins[] = {
  0,  // Blue
  1,  // Green
  3,  // Yellow
  4,  // Red
  5   // White
};

void ledClass::init(void){
  for(uint8_t i=0; i<sizeof(_pins); i++){
    pinMode(_pins[i],OUTPUT);
    digitalWrite(_pins[i],OFF);  // Default OFF state
  }
}

void ledClass::blink(uint8_t color, uint32_t milliseconds){
  digitalWrite(_pins[color], ON);
  delay(milliseconds);
  digitalWrite(_pins[color], OFF);
  delay(milliseconds);
}

void ledClass::f_cycle(void){
  for(uint8_t i; i<sizeof(_pins); i++){
    blink(i, 50);
  }
}

void ledClass::allOFF(void){
  for(byte i=0; i<sizeof(_pins); i++){   // If state is off then turn off all LEDs
    digitalWrite(_pins[i], OFF);
  }
}

void ledClass::allON(void){
  for(byte i=0; i<sizeof(_pins); i++){   // If state is off then turn off all LEDs
    digitalWrite(_pins[i], ON);
  }
}

//fclass fan = fclass();





