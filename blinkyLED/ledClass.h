#ifndef ledClass_h
#define ledClass_h

#include <Arduino.h>

#define OFF HIGH
#define ON LOW

class ledClass{
  public:
    ledClass();
    void init();
    void blink(uint8_t color, uint32_t milliseconds);
    static void allOFF();  // Can be called without class object
    static void allON();
    
    // LED function patterns
    void f_cycle(void);
    
    
  private:
    static const byte _pins[];
};

//extern ledClass motor;

#endif
