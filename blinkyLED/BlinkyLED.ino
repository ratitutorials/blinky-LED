/*

*/
#include <Arduino.h>
#include "ledClass.h"

#define NOT_AN_INTERRUPT -1  // Fixed error

const byte interruptPin = 2;
bool pwrState = true;
unsigned long tmpMillis = 0;
ledClass led = ledClass();

struct color_of_LED{
  uint8_t blue;
  uint8_t green;
  uint8_t yellow;
  uint8_t red;
  uint8_t white;
};

static void changePwrState(void){
 pwrState = !pwrState;
 if(!pwrState) ledClass::allOFF();
}

void changeLED(void){
  
}

void cmdButton(void){
  byte holdSec = 0;
  if(tmpMillis){
    holdSec = (millis() - tmpMillis) / 1000;
    tmpMillis = 0;
  }
  else{
    tmpMillis = millis();
    exit;
  }
  switch(holdSec){
    case 0:
      changeLED();
      break;
    case 1:
      changePwrState();
      break;
    default:
      changePwrState();
      break;
  }
}

// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), cmdButton, CHANGE);
}

// the loop function runs over and over again forever
void loop() {
  while(pwrState){
    led.f_cycle();
  }
}
